import React from 'react'

const CharacterItem = ({ item }) => {
  return (
    <div className='card'>
      <div className='card-inner'>
        <div className='card-front'>
          <img src={item.img} alt='' />
        </div>
        <div className='card-back'>
          <h1>{item.name}</h1>
          <ul>
            <li>
              <strong>Name:</strong> {item.portrayed}
            </li>
            <li>
              <strong>Serien Name:</strong> {item.name}
            </li>            
            <li>
              <strong>Nickname:</strong> {item.nickname}
            </li>
            <li>
              <strong>Beruf:</strong> {item.occupation}
            </li>            
            <li>
              <strong>Geburtstag:</strong> {item.birthday}
            </li>
            <li>
              <strong>Serien Status:</strong> {item.status}
            </li>
          </ul>
        </div>
      </div>
    </div>
  )
}

export default CharacterItem
